# Introduction

Implement Marvel Kata <https://nikeyes.github.io/MarvelKata/#/portada>


# Installation

-   Git clone this repository
    
    ```shell
    git clone https://gitlab.com/lll-tools/go/lll-go-marvel.git
    cd lll-go-marvel
    ```
-   Build
    
    ```shell
    go mod tidy
    go build
    ```
-   Run Ginkgo test
    
    ```shell
    cp ComicsNextWeek.js /tmp
    cd marvel
    ginkgo
    ```


# Description


## Define `Comic` and `ComicList` struct

```go
type Comic struct {
	Title string
	Urls  []struct {
		Url string
	}
	Thumbnail string
	Prices    []struct {
		Price string
	}
}

type ComicList struct {
	Data struct {
		Results []Comic
	}
}
```


## Define `ComicClient` interface and `ComicService`

-   Define function `DownloadNextWeekComics` to be implemented
    
    ```go
    type ComicClient interface {
    	DownloadNextWeekComics() ([]Comic, error)
    }
    ```
-   Define ComicService
    
    ```go
    type ComicService struct {
    	ComicClient ComicClient
    }
    
    func (c *ComicService) GetComics() ([]Comic, error) {
    	comics, err := c.ComicClient.DownloadNextWeekComics()
    	if err != nil {
    		log.Fatal(err)
    		fmt.Println(err.Error())
    	} else {
    		fmt.Println(len(comics))
    	}
    	return comics, err
    }
    ```


## Implement `DownloadNextWeekComics` in `ComicClient` services

-   Implement `DownloadNextWeekComics` in the mock service `ComicClientFile` to read from fake local file
    
    ```go
    type ComicClientFile struct {
    	FileName string
    }
    
    func (cf ComicClientFile) DownloadNextWeekComics() ([]Comic, error) {
    	var comicInfo ComicList
    
    	fakeFileComics := []Comic{
    		{
    			Title: "Spiderman",
    		},
    	}
    	data, err := os.ReadFile(cf.FileName)
    	if err != nil {
    		log.Fatal(err)
    	} else {
    		err = json.Unmarshal(data, &comicInfo)
    		if err != nil {
    			//fakeFileComics = append(fakeFileComics, comicInfo)
    			fakeFileComics = comicInfo.Data.Results
    		}
    	}
    	return fakeFileComics, err
    }
    ```
-   Implement `DownloadNextWeekComics` in the real service `ComicClientWeb` to read from real API call
    
    ```go
    func (cw ComicClientWeb) DownloadNextWeekComics() ([]Comic, error) {
    	fakeWebComics := []Comic{
    		{
    			Title: "Superman",
    		},
    	}
    	return fakeWebComics, nil
    }
    ```


## Use `ComicService` to isolate `ComicClient` implementation in tests execution

Uncommet the source code block to be used for running the tests

```go
var CS ComicService

func init() {
	// Fake tests
	CS = ComicService{
		ComicClient: ComicClientFake{},
	}
	// Mock tests
	CS = ComicService{
		ComicClient: ComicClientFile{"/tmp/ComicsNextWeek.js"},
	}
	// // Real tests
	// CS = ComicService{
	// 	ComicClient: ComicClientWeb{"http://gateway.marvel.com:80/v1/public/comics?dateDescriptor=nextWeek"},
	// }
}
```


## Create the Ginkgo test suite

```shell
ginkgo bootstrap
ginkgo generate marvel
```


## Implement the tests

-   (ShouldNot - BeNil) (ShouldNot - BeEmpty)
    
    ```go
    var nextWeekComics []marvel.Comic
    var downloadError error
    
    BeforeEach(func() {
    })
    
    Describe("Test Comic Service", func() {
    	Context("Comic List", func() {
    
    		It("Connection Should Not be nil", func() {
    			// Comic Service
    			Expect(marvel.CS).ShouldNot(BeNil())
    		})
    
    		It("Comic List Should Not be empty", func() {
    			// Comic Service
    			//Expect(marvel.CS).ShouldNot(BeNil())
    			//
    			nextWeekComics, downloadError = marvel.CS.GetComics()
    			if downloadError != nil {
    				fmt.Println(downloadError.Error())
    			} else {
    				Expect(nextWeekComics).ShouldNot(BeEmpty())
    			}
    		})
    
    		It("Comic Title Should Not be nil", func() {
    			for _, c := range nextWeekComics {
    				Expect(c.Title).ShouldNot(BeNil())
    			}
    		})
    
    	})
    })
    ```


# Todo

-   Define other tests
-   Improve ComicList struct to get Price
-   Implement API call
-   Example
    
        Conexión desde Servidor
        
        Url: http://gateway.marvel.com:80/v1/public/comics?dateDescriptor=nextWeek&ts=XXX&apikey=XXX&hash=XXX
        
        ts = timestamp o cualquier número.
        apiKey = public key
        hash = md5(ts+privateKey+publicKey)
        
        Conexión desde cliente:
        
        Url: http://gateway.marvel.com:80/v1/public/comics?dateDescriptor=nextWeek&apikey=XXXX
        
        Solo accesible desde los dominios:
        *.bcnswcraft.com
        *.developer.marvel.com
