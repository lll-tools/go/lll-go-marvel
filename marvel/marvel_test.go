package marvel_test

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/lll-tools/go/lll-go-marvel/marvel"
)

var _ = Describe("Marvel", func() {

	var nextWeekComics []marvel.Comic
	var downloadError error

	BeforeEach(func() {
	})

	Describe("Test Comic Service", func() {
		Context("Comic List", func() {

			It("Connection Should Not be nil", func() {
				// Comic Service
				Expect(marvel.CS).ShouldNot(BeNil())
			})

			It("Comic List Should Not be empty", func() {
				// Comic Service
				//Expect(marvel.CS).ShouldNot(BeNil())
				//
				nextWeekComics, downloadError = marvel.CS.GetComics()
				if downloadError != nil {
					fmt.Println(downloadError.Error())
				} else {
					Expect(nextWeekComics).ShouldNot(BeEmpty())
				}
			})

			It("Comic Title Should Not be nil", func() {
				for _, c := range nextWeekComics {
					Expect(c.Title).ShouldNot(BeNil())
				}
			})

		})
	})
})
