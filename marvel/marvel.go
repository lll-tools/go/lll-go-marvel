package marvel

import (
	"fmt"
	"log"
)

type Comic struct {
	Title string
	Urls  []struct {
		Url string
	}
	Thumbnail string
	Prices    []struct {
		Price string
	}
}

type ComicList struct {
	Data struct {
		Results []Comic
	}
}

type ComicClient interface {
	DownloadNextWeekComics() ([]Comic, error)
}

type ComicService struct {
	ComicClient ComicClient
}

func (c *ComicService) GetComics() ([]Comic, error) {
	comics, err := c.ComicClient.DownloadNextWeekComics()
	if err != nil {
		log.Fatal(err)
		fmt.Println(err.Error())
	} else {
		fmt.Println(len(comics))
	}
	return comics, err
}

var CS ComicService

func init() {
	// Fake tests
	CS = ComicService{
		ComicClient: ComicClientFake{},
	}
	// Mock tests
	CS = ComicService{
		ComicClient: ComicClientFile{"/tmp/ComicsNextWeek.js"},
	}
	// // Real tests
	// CS = ComicService{
	// 	ComicClient: ComicClientWeb{"http://gateway.marvel.com:80/v1/public/comics?dateDescriptor=nextWeek"},
	// }
}

func RunMarvel() {
	// Start
	fmt.Println("Marvel Start")
	// Print
	nextWeekComics, downloadError := CS.GetComics()
	if downloadError != nil {
		fmt.Println(downloadError.Error())
		log.Fatal(downloadError)
	} else {
		fmt.Printf("%+v", nextWeekComics)
		for _, c := range nextWeekComics {
			fmt.Println(c.Title)
			fmt.Println(c)
			fmt.Println("")
		}
	}

	// Stop
	fmt.Println("Marvel Stop")
}
