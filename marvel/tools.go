package marvel

import (
	"encoding/json"
	"log"
	"os"
	"time"
)

type ComicClientError struct {
	When time.Time
	What string
}

type ComicClientFake struct {
}

func (ck ComicClientFake) DownloadNextWeekComics() ([]Comic, error) {
	fakeFileComics := []Comic{
		{
			Title: "Spiderman",
		},
	}
	return fakeFileComics, nil
}

type ComicClientFile struct {
	FileName string
}

func (cf ComicClientFile) DownloadNextWeekComics() ([]Comic, error) {
	var comicInfo ComicList

	fakeFileComics := []Comic{
		{
			Title: "Spiderman",
		},
	}
	data, err := os.ReadFile(cf.FileName)
	if err != nil {
		log.Fatal(err)
	} else {
		err = json.Unmarshal(data, &comicInfo)
		if err != nil {
			//fakeFileComics = append(fakeFileComics, comicInfo)
			fakeFileComics = comicInfo.Data.Results
		}
	}
	return fakeFileComics, err
}

type ComicClientWeb struct {
	WebUrl string
}

func (cw ComicClientWeb) DownloadNextWeekComics() ([]Comic, error) {
	fakeWebComics := []Comic{
		{
			Title: "Superman",
		},
	}
	return fakeWebComics, nil
}

// fmt.Println("Marvel List")

// var comicList interface{}

// cf := ComicClientFile{
// 	FileName: "/tmp/ComicsNextWeek.js",
// }
// data, err := os.ReadFile(cf.FileName)
// if err != nil {
// 	log.Fatal(err)
// } else {
// 	err = json.Unmarshal(data, &comicList)
// 	if err != nil {
// 		fmt.Printf("%+v", comicList)
// 		comicListData := comicList.(map[string]interface{})
// 		for k, v := range comicListData {
// 			switch vv := v.(type) {
// 			case string:
// 				fmt.Println(k, "is string", vv)
// 			case float64:
// 				fmt.Println(k, "is float64", vv)
// 			case []interface{}:
// 				fmt.Println(k, "is an array:")
// 				for i, u := range vv {
// 					fmt.Println(i, u)
// 				}
// 			default:
// 				fmt.Println(k, "is of a type I don't know how to handle")
// 			}
// 		}
// 		fmt.Println(len(comicListData))
// 	}
// }
